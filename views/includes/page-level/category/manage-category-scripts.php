<script src="<?=BASEASSETS;?>js/plugins/toastr/toaster.min.js"></script>
<script src="<?=BASEASSETS;?>vendor/datatables/jquery.dataTables.js"></script>
<script src="<?=BASEASSETS;?>vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?=BASEASSETS;?>js/pages/category/manage-category.js"></script>

<script>
	toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	<?php
		if(Session::hasSession(ADD_SUCCESS)):
	?>
			toastr.success("New cateory has been added successfully");
	<?php
			Session::unsetSession(ADD_SUCCESS);
		elseif(Session::hasSession(ADD_ERROR)):
	?>
			toastr.error("Adding a category failed");
	<?php
			Session::unsetSession(ADD_ERROR);
		elseif(Session::hasSession('csrf')):
	?>
			toastr.error("Unauthorized Access,Token match","Token error!");
	<?php
			Session::unsetSession('csrf');
		endif;
	?>
</script>