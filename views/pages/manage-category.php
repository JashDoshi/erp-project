<?php
require_once __DIR__."/../../helper/init.php";
$page_title = "Quick ERP | Manage Catagory";
$sidebarSection = 'category';
$sidebarSubSection = 'manage';

$sidebarTitle = "Quick ERP";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  
  <?php
  require_once __DIR__."/../includes/head-section.php";
  ?>
  <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>css/plugins/toastr/toaster.min.css">
  <link rel="stylesheet" type="text/css" href="<?=BASEASSETS;?>vendor/datatables/datatables.bootstrap4.min.css">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__."/../includes/sidebar.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__."/../includes/navbar.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <h1 class="h3 mb-4 text-gray-888">Manage Catagory</h1>
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
            </div>
            <div class="card-body">
              <table class="table table-bordered table-responsive" id="manage-category-table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Category Name</th>
                    <th>Actions</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php
      require_once __DIR__."/../includes/footer.php";
      ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <?php
  require_once __DIR__."/../includes/scroll-top.php";
  ?>

  <!-- Logout Modal-->
  

  <?php
  require_once __DIR__."/../includes/core-scripts.php";
  ?>
  <?php
  require_once __DIR__."/../includes/index-scripts.php";
  ?>
  <?php
  require_once __DIR__."/../includes/page-level/category/manage-category-scripts.php";
  ?>
  

</body>

</html>
