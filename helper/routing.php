<?php
require_once "init.php";
if(isset($_POST['add_category']))
{
	if(Util::verifyCSRFToken($_POST))
	{
		$result = $di->get('category')->addCategory($_POST);
		switch($result)
		{
			case ADD_ERROR:
				Session::setSession(ADD_ERROR,"Add Category Error!");
				Util::redirect("manage-category.php");
				break;
			case ADD_SUCCESS:
				Session::setSession(ADD_SUCCESS,"Add Category Success!");
				Util::redirect("manage-category.php");
				break;
			case VALIDATION_ERROR:
				Session::setSession('validation',"Validation Error");
				Session::setSession('old',$_POST);
				Session::setSession('errors',serialize($di->get('category')->getValidator()->errors()));
				Util::redirect("add-category.php");
				break;
		}
	}
	else
	{
		Session::setSession("csrf","CSRF ERROR");
		Util::redirect("manage-category.php");//need to redirect to some error page
	}
}
if(isset($_POST['page']))
{
	$di->get('category')->getJSONDataForDataTable();
}