var TableDatatables = function(){
	var handleCategoryTable = function(){
		var manageCategoryTable = $("#manage-category-table");
		var baseURL = window.location.origin;
		var filePath = "/helper/routing.php";
		manageCategoryTable.dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: baseURL+filePath,
				type: "POST",
				data: {
					"page": "manage_category"
				}
			}
		})
	}
	return {
		//main function to handle all the datatables
		init: function(){
			handleCategoryTable();
		}
	}
}();
jQuery(document).ready(function(){
	TableDatatables.init();
});