<?php
class Category
{
	private $table = "category";
	private $columns = ['id','name'];
	protected $di;
	private $database;
	private $validator;
	public function __construct(DependencyInjector $di){
		$this->di = $di;
		$this->database = $this->di->get('database');
	}
	public function getValidator()
	{
		return $this->validator;
	}
	public function validateData($data)
	{
		$this->validator = $this->di->get('validator');
		$this->validator = $this->validator->check($data,[
			'name'=>[
				'required'=>true,
				'minlength'=>3,
				'maxlength'=>28,
				'unique'=>$this->table
			]
		]);
	}
	public function addCategory($data)
	{
		//validate data
		$this->validateData($data);

		//insert data in database
		if(!$this->validator->fails())
		{
			try
			{
				$this->database->beginTransaction();
				$data_to_be_inserted = ['name'=>$data['name']];
				$category_id = $this->database->insert($this->table,$data_to_be_inserted);
				$this->database->commit();
				return ADD_SUCCESS;
			}catch(Exception $e){
				$this->database->rollBack();
				return ADD_ERROR;
			}
		}
		return VALIDATION_ERROR;
	}
	public function getJSONDataForDataTable()
	{
		$query = "SELECT * FROM {$this->table} WHERE deleted = 0";
		$fetchData = $this->database->raw($query);
		$data = [];
		$numRows = is_array($fetchData) ? count($fetchData) : 0;
		for($i=0;$i<$numRows;$i++)
		{
			$subArray = [];
			$subArray[] = $i+1;
			$subArray[] = $fetchData[$i]->name;
			$subArray[] = <<<BUTTONS
<button class='btn btn-outline-primary btn-sm' data-id='{$fetchData[$i]->id}'><i class='fas fa-pencil-alt'></i></button> 
<button class='btn btn-outline-danger btn-sm' data-id='{$fetchData[$i]->id}'><i class='fas fa-trash-alt'></i></button> 
BUTTONS;
			$data[] = $subArray;
		}
		$output = array(
			'draw'=>1,
			'recordsTotal'=>10,
			'recordsFiltered'=>5,
			'data'=>$data,
		);
		echo json_encode($output);
	}
}